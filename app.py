from flask import Flask, render_template, request, send_from_directory, redirect, url_for
import requests
import os
from huggingface_hub import ModelFilter, HfApi, hf_hub_download
from huggingface_hub.hf_api import ModelInfo
import sys



app = Flask(__name__)

# Replace with your Hugging Face API key if needed
# HUGGINGFACE_API_KEY = 'YOUR_HUGGINGFACE_API_KEY'

@app.route('/')
def index():
    # Fetch available models from Hugging Face Model Hub
    new_filter = ModelFilter(author="AI4MIRO")
    api = HfApi()
    models = api.list_models(filter=new_filter)
    print("list models",models)
    # models = get_available_models()
    return render_template('index.html', models=models)

@app.route('/download_model', methods=['POST'])
def download_model():
    selected_model = request.form.get('selected_model')
    selected_model_id = selected_model.split("id='")[1].split("'")[0] # moche
    print('selected_model',selected_model, file=sys.stdout)
    print('selected_model.id',selected_model_id, file=sys.stdout)
    hf_hub_download(repo_id=selected_model_id,
                    filename="best_metric_model_0.pth",
                    local_dir = 'C:/Users/huetm/Documents/CHARP/Website/test_list_models/model_download'
                    )
    return redirect(url_for('index'))
    # if selected_model:
    #     # Download the selected model
    #     download_url = get_model_download_url(selected_model)
    #     download_path = download_model_file(download_url, selected_model)
    #     return send_from_directory('models', download_path, as_attachment=True)
    # else:
    #     return redirect(url_for('index'))

def get_available_models():
    # Fetch the list of available models from Hugging Face Model Hub
    url = 'https://huggingface.co/models'
    # headers = {'Authorization': f'Bearer {HUGGINGFACE_API_KEY}'} if HUGGINGFACE_API_KEY else {}
    response = requests.get(url)#, headers=headers)

    if response.status_code == 200:
        return response.json()
    else:
        return []

def get_model_download_url(model_name):
    # Fetch the download URL for the selected model
    url = f'https://api-inference.huggingface.co/models/{model_name}/download'
    # headers = {'Authorization': f'Bearer {HUGGINGFACE_API_KEY}'} if HUGGINGFACE_API_KEY else {}
    response = requests.get(url)#, headers=headers)

    if response.status_code == 200:
        return response.json().get('url')
    else:
        return None

def download_model_file(url, model_name):
    # Download the model file and save it to the 'models' directory
    response = requests.get(url)

    if response.status_code == 200:
        download_path = f'{model_name}.zip'
        with open(os.path.join('models', download_path), 'wb') as file:
            file.write(response.content)
        return download_path
    else:
        return None

if __name__ == '__main__':
    app.run(debug=True)
