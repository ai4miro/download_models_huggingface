# Download_models_huggingface
Retrieves a list of models available in the organization : https://huggingface.co/AI4MIRO

It uses the function hf_hub_download from HfApi. Documentation: https://huggingface.co/docs/huggingface_hub/en/guides/download

## Run Flask app
```
flask run
```